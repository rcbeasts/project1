package t1;

import static t1.FTP19Packet.ACK;
import static t1.FTP19Packet.DATA;
import static t1.FTP19Packet.FIN;
import static t1.FTP19Packet.MAX_FTP19_PACKET_SIZE;
import static t1.FTP19Packet.UPLOAD;

import java.io.FileInputStream;
import java.io.IOException;
import java.net.*;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.TimeUnit;

import lib.Stats;

/**
 * @author Frederico Lopes 42764 Tomas Silva 48170
 *
 */
public class FTP19Client {

	static final int FTP19_PORT = 9000;

	static final int DEFAULT_TIMEOUT = 500;
	private static final int DEFAULT_BLOCK_SIZE = 8 * 1024;

	static int windowSize, lastBytes;
	static int blockSize = DEFAULT_BLOCK_SIZE;
	static int timeout = DEFAULT_TIMEOUT;

	/* PARA GBN (seqN,0,buffer,n) para SR (0,seqN,buffer,n) */
	static Stats stats;
	static BlockingQueue<FTP19Packet> receiverQueue;
	static SocketAddress srvAddress;

	static List<FTP19Packet> packetsToSend;// NAO ESTA A GUARDAR O TAMANHO CERTO, FAZER <K,V>
	static List<Long> timeoutChanger;
	static List<Long> notAckedQueue;
	static List<Long> retriedPackets;
	static long lastAcked;

	/**
	 * Receiver thread so that ACKs from server can be received by this at the same
	 * time the main thread is sending data to the server or reading file. ACKs are
	 * added to a shared concurrent queue. The server port is updated if it changes
	 * during upload.
	 */
	static class Receiver implements Runnable {

		DatagramSocket socket;

		Receiver(DatagramSocket sock) {
			socket = sock;
		}

		public void run() {
			try {
				for (;;) {
					byte[] buffer = new byte[MAX_FTP19_PACKET_SIZE];
					DatagramPacket msg = new DatagramPacket(buffer, buffer.length);
					socket.receive(msg);
					// update server address (it changes when the reply to UPLOAD
					// comes from a different port)
					srvAddress = msg.getSocketAddress();
					// make the packet available to sender process
					FTP19Packet pkt = new FTP19Packet(msg.getData(), msg.getLength());
					receiverQueue.put(pkt);
				}
			} catch (Exception e) {

				System.out.println("Receiver done.");
			}
		}

	}

	static FTP19Packet buildUploadPacket(String filename) {
		return new FTP19Packet().putShort(UPLOAD).putLong(0L).putLong(0L).putString(filename);
	}

	static FTP19Packet buildDataPacket(long cseqN, long sseqN, byte[] payload, int length) {
		return new FTP19Packet().putShort(DATA).putLong(cseqN).putLong(sseqN).putBytes(payload, length);
	}

	static FTP19Packet buildFinPacket(long seqN) {
		return new FTP19Packet().putShort(FIN).putLong(seqN).putLong(seqN);
	}

	/**
	 * envia um upload request com o nome do file
	 * 
	 * @param socket
	 * @param pkt
	 * @throws IOException
	 * @throws InterruptedException
	 * @throws Exception
	 */
	static void sendUploadRequest(DatagramSocket socket, FTP19Packet pkt, long expectedACK) throws Exception {

		long sendTime = System.currentTimeMillis();
		DatagramPacket dgpkt = pkt.toDatagram(srvAddress);
		socket.send(dgpkt);

		FTP19Packet ack = receiverQueue.poll(timeout, TimeUnit.MILLISECONDS);
		if (ack != null)
			if (ack.getShort() == ACK)
				if (expectedACK == ack.getLong()) {
					stats.newRTTMeasure(System.currentTimeMillis() - sendTime); // RTT
					//adjustTimeout(System.currentTimeMillis() - sendTime);
				} else
					System.err.println("got wrong ack: " + ack);
			else
				System.err.println("got unexpected packet: " + ack);
		else
			System.err.println(expectedACK + " timed out waiting for " + srvAddress);

	}

	/**
	 * envia o ultimo pacote
	 * 
	 * @param socket
	 * @param pkt
	 * @throws Exception
	 */
	static void sendFinRequest(DatagramSocket socket, FTP19Packet pkt, long expectedACK) throws Exception {
		long sendTime = System.currentTimeMillis();
		DatagramPacket dgpkt = pkt.toDatagram(srvAddress);
		socket.send(dgpkt);

		FTP19Packet ack = receiverQueue.poll(timeout, TimeUnit.MILLISECONDS);
		if (ack != null)
			if (ack.getShort() == ACK)
				if (expectedACK == ack.getLong()) {
					stats.newRTTMeasure(System.currentTimeMillis() - sendTime); // RTT
				} else
					System.err.println("got wrong ack: " + ack);
			else
				System.err.println("got unexpected packet: " + ack);
		else
			System.err.println(expectedACK + " timed out waiting for " + srvAddress);
	}

	/**
	 * GBN-> SENDS ALL WINDOWED PACKETS AGAIN | SR-> SENDS THE PACKET AGAIN
	 * 
	 * @return SSeqN from ACK
	 */
	static void sendRetry(DatagramSocket socket, int expectedACK) throws Exception {

		for (int i = expectedACK - 1; i < windowSize + expectedACK && i < packetsToSend.size(); i++) {
			DatagramPacket pkt = packetsToSend.get(i).toDatagram(srvAddress);
			socket.send(pkt);
			if (i == packetsToSend.size() - 1) {
				stats.newPacketSent(lastBytes);
			} else {
				stats.newPacketSent(blockSize);
			}
			retriedPackets.add((long) expectedACK);
		}
	}

	// ESTOU A MANDAR MAL O EXPECTEDACK DE CERTEZA BURRO
	/**
	 * so muda timeout quando nao e retry
	 * 
	 * 
	 * GBN -> receiverQueue.poll(timeout, TimeUnit.MILLISECONDS); SR ->
	 * receiverQueue.poll(timeout(doExpectedACK),* TimeUnit.MILLISECONDS);
	 * 
	 * @param socket
	 * @param retry
	 * @throws Exception
	 */
	public static void receiveAck(DatagramSocket socket) throws Exception {
		boolean in = false;
		long sendTime = System.currentTimeMillis();
		long expectedACK = notAckedQueue.get(0);
		FTP19Packet ack = receiverQueue.poll(timeout, TimeUnit.MILLISECONDS);
		if (ack != null)
			if (ack.getShort() == ACK)
				if (ack.getLong() < windowSize + expectedACK) {
					adjustTimeout(System.currentTimeMillis() - sendTime);
					stats.newRTTMeasure(System.currentTimeMillis() - sendTime); // RTT
					lastAcked = expectedACK;
					notAckedQueue.remove(ack.getLong());
					in = true;
				} else {
					System.err.println("got wrong ack: " + ack);
					if (notAckedQueue.get(0) == ack.getLong()) {
						notAckedQueue.clear();
					}
				}
			else
				System.err.println("got unexpected packet: " + ack);
		else {
			System.err.println(expectedACK + " timed out waiting for " + srvAddress);
		}

		if (!in) {
			sendRetry(socket, (int) expectedACK);
		}
	}

	static void sendFile(String filename) throws Exception {
		try (DatagramSocket socket = new DatagramSocket(); // for testing use lib.RCDatagramSocket();
				FileInputStream f = new FileInputStream(filename)) {

			stats = new Stats(windowSize, timeout);

			// create concurrent producer/consumer queue for ACKs
			receiverQueue = new ArrayBlockingQueue<>(windowSize);

			// start a receiver process to feed the queue
			new Thread(new Receiver(socket)).start();

			System.out.println("sending file: \"" + filename + "\" to server: " + srvAddress);

			// Upload
			sendUploadRequest(socket, buildUploadPacket(filename), 0L);
			System.out.println("continuing to server: " + srvAddress + " with blocksize: " + blockSize);

			long seqN = 1L; // data block count starts at 1
			int n;
			byte[] buffer = new byte[blockSize];
			while ((n = f.read(buffer)) > 0) {
				packetsToSend.add(buildDataPacket(seqN, 0L, buffer, n));
				stats.newPacketSent(n);
				seqN++;
				lastBytes = n;
			}
			seqN = 1L;
			while (lastAcked < packetsToSend.size()) {
				if (seqN - lastAcked <= windowSize && seqN <= packetsToSend.size()) {
					for (int i = (int) (seqN - 1); i < windowSize + lastAcked && i < packetsToSend.size(); i++) {
						DatagramPacket pkt = packetsToSend.get(i).toDatagram(srvAddress);
						socket.send(pkt);
						notAckedQueue.add(seqN);
						seqN++;
					}
				} else {
					while (!notAckedQueue.isEmpty()) {
						receiveAck(socket);
					}
				}
			}
			// FIN
			FTP19Packet pk = buildFinPacket(seqN);
			System.out.println("sending: " + pk);
			sendFinRequest(socket, pk, seqN);

			System.out.println("transfer finished.");
			stats.printReport();
		}
	}

	// -------------------------------------METODOS_PRIVADOS-----------------------------//

	private static void adjustTimeout(long newTimeout) {
		long tmp = 0;
		if (!timeoutChanger.isEmpty())
			tmp = timeoutChanger.get(0);
		timeoutChanger.add(newTimeout + tmp);
		for (int i = 0; i < timeoutChanger.size(); i++) {
			tmp += timeoutChanger.get(i);
		}
		timeout = (int) (tmp / timeoutChanger.size());
		stats.newTimeout(timeout);
	}

	/*
	 * private static void adjustWindowSize() { if
	 * (timeoutChanger.get(timeoutChanger.size() - 1) <
	 * timeoutChanger.get(timeoutChanger.size() - 2)) { windowSize++; } else { if
	 * (windowSize > 1) { windowSize--; } } stats.newWindowSize(windowSize); }
	 */

	public static void main(String[] args) throws Exception {

		try {
			switch (args.length) {
			case 4:
				windowSize = Integer.parseInt(args[3]);
				// windowSize must be at least 1
				if (windowSize <= 0)
					throw new Exception("wrong window size");
			case 3:
				blockSize = Integer.parseInt(args[2]);
				// blockSize must be at least 1 and less than MAX_FTP19_PACKET_SIZE
				if (blockSize <= 0 || blockSize > MAX_FTP19_PACKET_SIZE)
					throw new Exception("wrong block size");
			case 2:
				break;
			default:
				throw new Exception("bad parameters");
			}
			lastAcked = 0L;
			packetsToSend = new ArrayList<FTP19Packet>();
			timeoutChanger = new ArrayList<Long>();
			notAckedQueue = new ArrayList<Long>();
			retriedPackets = new ArrayList<Long>();
			String filename = args[0];
			srvAddress = new InetSocketAddress(InetAddress.getByName(args[1]), FTP19_PORT);
			sendFile(filename);
		} catch (IOException x) {
			System.err.println(x);
			System.exit(1);
		} catch (Exception x) {
			System.err.println(x);
			StackTraceElement[] stk = x.getStackTrace();
			System.err.printf("usage: java " + stk[stk.length - 1].getClassName()
					+ " filename server [ blocksize [ windowsize ]]\n");
			System.exit(1);
		}

	}

}
