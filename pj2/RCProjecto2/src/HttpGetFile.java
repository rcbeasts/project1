import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.RandomAccessFile;
import java.net.Socket;
import java.net.URL;

public class HttpGetFile {
	public static void main(String[] args) throws Exception {
		if (args.length != 1) {
			System.out.println("Usage: java HttpClientDemo url_to_access");
			System.exit(0);
		}
		String url = args[0];
		URL u = new URL(url);

		// Assuming URL of the form http://server-name/path ....
		int port = u.getPort() == -1 ? 80 : u.getPort();
		String path = u.getPath() == "" ? "/" : u.getPath();
		Socket sock = new Socket(u.getHost(), port);
		
		String absolutePath = new File(".").getAbsolutePath();
		int last = absolutePath.length() - 1;
		absolutePath = absolutePath.substring(0, last);
		String fileName = "Downloads\\Download.mp4";

		File file = new File(absolutePath + fileName);
		File fileReceived = new File(absolutePath + path);
		RandomAccessFile f = new RandomAccessFile(fileReceived, "rw");

		FileOutputStream fo = new FileOutputStream(file);
		OutputStream bw = fo;

		int mark = HttpLazyServer.MAX_BYTES - 1;// pos do ultimo byte a receber do server
		
		System.out.println(fileReceived.length());

		while (f.getFilePointer() < fileReceived.length()) {// enquanto nao enviou o tamanho dp ficheiro, continua a
															// enviar

			System.out.println("Init= " + f.getFilePointer() + " Mark= " + mark);

			OutputStream out = sock.getOutputStream();
			InputStream in = sock.getInputStream();

			long[] range = Http
					.parseRangeValues("Range: bytes=" + (int) f.getFilePointer() + "-" + mark + "\r\n");
			String sentRange = "Range: bytes=" + range[0] + "-" + range[1] + "\r\n";

			String request = String.format(
					"GET %s HTTP/1.0\r\n" + "Host: %s\r\n" + sentRange + "User-Agent: X-RC2018\r\n\r\n", path,
					u.getHost());
			out.write(request.getBytes());

			System.out.println("\nSent:\n\n" + request);
			System.out.println("Got:\n");
			String answerLine = Http.readLine(in);
			System.out.println(answerLine);

			// bw.write((answerLine + "\n").getBytes());

			answerLine = Http.readLine(in);
			while (!answerLine.equals("")) {
				System.out.println(answerLine);
				answerLine = Http.readLine(in);
			}

			// bw.write((answerLine + "\n").getBytes());

			System.out.println("\nPayload:\n");

			int n;
			byte[] buffer = new byte[80];
			try {
				while ((n = in.read(buffer)) != -1) {
					// System.out.print(new String(buffer, 0, n));
					bw.write(buffer, 0, n);
				}
			} catch (Exception e) {
				e.printStackTrace();
			};
			
			

			if (mark == fileReceived.length()) {
				System.out.println("Terminou");
				break;
			} else {// fechar e voltar a abrir a conexao com o server
				System.out.println("entrou no else");
				out.close();
				in.close();
				sock.close();// termina a conexao
				sock = new Socket(u.getHost(), u.getPort());

				f.seek(mark + 1);// atualiza o offset
				mark += (fileReceived.length() - 1 - mark);// atualiza a pos do ultimo byte a receber
			}

		} // fim do while
		bw.close();
		sock.close();

	}

}
