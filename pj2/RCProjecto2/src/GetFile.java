
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.RandomAccessFile;
import java.net.Socket;
import java.net.URL;

/**
 * @author Frederico Lopes 42764 Tomas Silva 48170
 *
 */
public class GetFile {

	private static final int BUF_SIZE = 512;
	private static final int SENDSIZE = 999999;
	private static int port;
	private static int numberOfServers;

	public static void main(String[] args) throws Exception {
		if (args.length != 1) {
			System.out.println("Usage: java HttpClientDemo url_to_access");
			System.exit(0);
		}
		String url = args[0];
		URL u = new URL(url);
		// Assuming URL of the form http://server-name:port/path ....
		port = u.getPort() == -1 ? 80 : u.getPort();
		String path = u.getPath() == "" ? "/" : u.getPath();
		numberOfServers = args.length;

		// absolutePath do file
		String absolutePath = new File(".").getAbsolutePath();
		int last = absolutePath.length() - 1;
		absolutePath = absolutePath.substring(0, last);
		String ext = path.split("\\.")[1];
		File fileToDownload = new File(absolutePath + path);
		File fileToFill = new File(absolutePath + "/download." + ext);
		// acessFile
		RandomAccessFile f = new RandomAccessFile(fileToDownload, "rw");

		// Verify if a file is partially downloaded
		boolean append = false;
		if (fileToFill.exists() && (fileToFill.length() < fileToDownload.length())) {
			append = true;
			f.seek(fileToFill.length());
		}
		FileOutputStream fo = new FileOutputStream(fileToFill, append);
		OutputStream bw = fo;// stream to fill the downloaded file (fileToFill)
		Stats stats = new Stats();

		// offset do contentLength
		long offset = f.getFilePointer() + SENDSIZE;
		long received = 0;
		while (fileToFill.length() < fileToDownload.length()) {
			Socket sock = new Socket(u.getHost(), port);
			OutputStream out = sock.getOutputStream();
			InputStream in = sock.getInputStream();

			// range of the content to download
			long[] range = Http.parseRangeValues("bytes=" + f.getFilePointer() + "-" + offset);
			String sentRange = "Range: bytes=" + range[0] + "-" + range[1] + "\r\n";

			// client request
			String request = String.format(
					"GET %s HTTP/1.0\r\n" + "Host: %s\r\n" + sentRange + "User-Agent: X-Client-Demo-RC2019\r\n\r\n",
					path, u.getHost());
			out.write(request.getBytes());

			System.out.println("\nSent:\n\n" + request);
			System.out.println("Got:\n");

			// server reply
			String answerLine = Http.readLine(in); // first line is always present
			System.out.println(answerLine);
			answerLine = Http.readLine(in);
			while (!answerLine.equals("")) {
				System.out.println(answerLine);
				answerLine = Http.readLine(in);
			}
			// System.out.println("\nPayload:\n");
			// payload
			int n, total = 0;
			byte[] buffer = new byte[BUF_SIZE];
			while ((n = in.read(buffer)) >= 0) {
				bw.write(buffer, 0, n);
				total += n;
			}
			received += total;
			stats.newRequest(total);
			f.seek(f.getFilePointer() + total);

			// change offset
			if (f.getFilePointer() + SENDSIZE > fileToDownload.length())
				offset += fileToDownload.length() - f.getFilePointer();
			else
				offset = f.getFilePointer() + SENDSIZE;

			changePort();// changes the port of the connection (server)
			sock.close();
			System.out.println("received = " + received);
		}
		bw.close();
		stats.printReport();
	}

	private static void changePort() {
		if (port == 8080 + numberOfServers - 1)
			port = 8080;
		else
			port++;
	}

}
